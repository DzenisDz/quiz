import Repository from 'axios';

const resource = '/MultipleChoiceQuestion';

export default {
    get() {
        return Repository.get(`${resource}`);
    },

    getForQuiz(quizId){
        return Repository.get(`quizzes/${quizId}${resource}`);
    },

    getMultipleChoiceQuestion(quizId) {
        return Repository.get(`${resource}/${quizId}`);
    },

    createMultipleChoiceQuestions(payload) {
        return Repository.post(`${resource}`, payload);
    },
}