import Repository from 'axios';

const resource = '/points';

export default {
    get() {
        return Repository.get(`${resource}`);
    },

    getForQuiz(quizId){
        return Repository.get(`quizzes/${quizId}${resource}`);
    },

    getPoints(quizId) {
        return Repository.get(`${resource}/${quizId}`);
    },

    createPoints(payload) {
        return Repository.post(`${resource}`, payload);
    },
}