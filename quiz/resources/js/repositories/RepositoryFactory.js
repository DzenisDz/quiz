import QuizRepository from './QuizRepository';
import QuestionRepository from "./QuestionRepository";
import AnswerRepository from "./AnswerRepository";
import PointsRepository from "./PointsRepository";
import MultipleChoiceQuestionRepository from "./DropDownQuestionRepository"
const repositories = {
    quizzes: QuizRepository,
    questions: QuestionRepository,
    answers: AnswerRepository,
    points: PointsRepository,
    dropDownQuestoiona: MultipleChoiceQuestionRepository
    // other repositories...
};

export const RepositoryFactory = {
    get: name => {
        const repository = repositories[name];
        return repository;
    }
};