<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMultipleChoiceQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MultipleChoiceQuestion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('quiz_id');
            $table->string('title', 255);
            $table->unsignedBigInteger('Dropdown1_ID')->nullable(false);
            $table->unsignedBigInteger('Dropdown2_ID')->nullable(false);
            $table->text('AnswerText')->nullable(false);
            

            $table->timestamps();
            
            $table->foreign('quiz_id')->references('id')->on('quizzes')->onDelete('cascade');
            $table->foreign('Dropdown1_ID')->references('id')->on('DropdownTexts');
            $table->foreign('Dropdown2_ID')->references('id')->on('DropdownTexts');
        });

        Schema::create('DropdownTexts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('MCQ_id');
            $table->string('text', 255);
            $table->unsignedBigInteger('Dropdown_ID')->nullable(false);

            $table->timestamps();
            
            $table->foreign('MCQ_id')->references('id')->on('MultipleChoiceQuestion')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MultipleChoiceQuestion');
        Schema::dropIfExists('DropdownTexts');
    }
}
