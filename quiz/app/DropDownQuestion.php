<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class DropDownQuestion extends Model
{
    protected $fillable = [
        'quiz_id',
        'title',
        'Dropdown1_ID',
        'Dropdown2_ID',
        'AnswerText'
    ];

    public function scopeOfQuiz(Builder $query, Quiz $quiz)
    {
        return $query->where('quiz_id', $quiz->id)->get();
    }

    public function quizzes()
    {
        return $this->belongsTo(Quiz::class);
    }
}
