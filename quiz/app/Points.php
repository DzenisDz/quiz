<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Points extends Model
{
    protected $fillable = [
        'quiz_id',
        'user_id',
        'name',
        'points',
        'date'
    ];

    public function scopeOfQuiz(Builder $query, Quiz $quiz)
    {
        return $query->where('quiz_id', $quiz->id)->OrderByDesc('points')->groupBy('user_id')->get(['quiz_id', 'user_id', 'name', 'points', 'date']);
    }

    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function quizzes()
    {
        return $this->belongsTo(Quiz::class);
    }
}
