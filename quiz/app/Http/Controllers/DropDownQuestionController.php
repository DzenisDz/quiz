<?php

namespace App\Http\Controllers;

use App\Quiz;
use App\DropDownQuestion;
use Illuminate\Http\Request;

class DropDownQuestionController extends Controller
{
    /**
     * PointsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $points = Points::all();

        return response($points);
    }

    public function indexForQuiz(DropDownQuestion $DropDownQuestion){
        $points = DropDownQuestion::ofQuiz($DropDownQuestion);
        
        return response($points);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'quiz_id' => 'required',
            'title' => 'required',
            'AnswerText' => 'required'
        ]);

        $point = new Points([
            'quiz_id' => $request->get('quiz_id'),
            'title' => $request->get('quiz_id'),
            'Dropdown1_ID' => $request->get('quiz_id'),
            'Dropdown2_ID' => $request->get('quiz_id'),
            'AnswerText' => $request->get('quiz_id')
        ]);

        $point->save();

        return response($point);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Points $point
     * @return \Illuminate\Http\Response
     */
    public function show(DropDownQuestion $DropDownQuestion)
    {
        return response($DropDownQuestion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\DropDownQuestion $point
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DropDownQuestion $DropDownQuestion)
    {
        $DropDownQuestion->fill($request->all());
        $DropDownQuestion->save();

        return response(null, 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\DropDownQuestion $point
     * @return \Illuminate\Http\Response
     */
    public function destroy(Points $DropDownQuestion)
    {
        $DropDownQuestion->delete();
        return response(null, 204);
    }
}
