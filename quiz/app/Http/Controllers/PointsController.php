<?php

namespace App\Http\Controllers;

use App\Quiz;
use App\Points;
use Illuminate\Http\Request;

class PointsController extends Controller
{
    /**
     * PointsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $points = Points::all()->orderBy('points');

        return response($points);
    }

    public function getOrderedByPoints(Quiz $quiz){
        $points = Points::ofQuiz($quiz);
        
        return response($points);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'quiz_id' => 'required',
            'user_id' => 'required',
            'points' => 'required'
        ]);

        $point = new Points([
            'quiz_id' => $request->get('quiz_id'),
            'user_id' => $request->get('user_id'),
            'name' => $request->get('name'),
            'points' => $request->get('points'),
            'date' => $request->get('date')
        ]);

        $point->save();

        return response($point);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Points $point
     * @return \Illuminate\Http\Response
     */
    public function show(Points $point)
    {
        return response($point);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Points $point
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Points $point)
    {
        $point->fill($request->all());
        $point->save();

        return response(null, 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Points $point
     * @return \Illuminate\Http\Response
     */
    public function destroy(Points $point)
    {
        $point->delete();
        return response(null, 204);
    }
}
